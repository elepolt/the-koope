# Home Assistant
[Installation Instructions](https://www.home-assistant.io/getting-started/)

## configuration.yml
Note: Update your resource url with your pi's IP address
```
  - platform: rest
    name: koope_door
    resource: "http://10.0.0.127:5000"
    json_attributes:
      - door
    scan_interval: 30
    value_template: "OK"
  - platform: template
    sensors:
      koope_door_state:
        value_template: "{{ state_attr('sensor.koope_door', 'door') ['state'] }}"
        icon_template: >
          {% set door_state = states('sensor.koope_door_state') %}
          {% if door_state == 'closed' %}
            mdi:door-closed-lock
          {% elif door_state == 'open' %}
            mdi:door-open
          {% else %}
            mdi:door
          {% endif %}
      koope_door_confidence:
        value_template: "{{ state_attr('sensor.koope_door', 'door') ['confidence_level'] }}"
        icon_template: >
          {% set door_state = states('sensor.koope_door_confidence') %}
          {% if door_state == 'confident' %}
            mdi:thumb-up
          {% else %}
            mdi:thumbs-up-down
          {% endif %}
rest_command:
  open_koope:
    url: "http://10.0.0.127:5000/door/open"
  close_koope:
    url: "http://10.0.0.127:5000/door/close"

shell_command:
  take_koope_picture: curl -o /config/www/koope.jpg http://10.0.0.127:5000/get_image

camera:
  - platform: local_file
    file_path: /config/www/koope.jpg
```

## Card Details
Note: This requires you to install both [HACS](https://hacs.xyz/) and [customer-cards/button-card](https://github.com/custom-cards/button-card)
```
type: vertical-stack
title: The Koope
cards:
  - type: custom:button-card
    entity: sensor.koope_door_state
    color: red
    tap_action:
      action: call-service
      service: |
        [[[
          var currentState = states["sensor.koope_door_state"].state;
          if (currentState == 'closed')
            return 'rest_command.open_koope'
          else if (currentState == 'open')
            return 'rest_command.close_koope'
        ]]]
    styles:
      icon:
        - color: |
            [[[
              if (states["sensor.koope_door_confidence"].state == 'confident')
                return 'rgb(19, 221, 41)'
              return 'rgb(255, 216, 51)'
            ]]]
    show_name: false
  - type: picture-elements
    elements: []
    camera_image: camera.local_file
  - type: horizontal-stack
    cards:
      - type: entity
        entity: sensor.koope_door_state
        name: Door
      - type: entity
        entity: sensor.koope_door_confidence
        name: Confidence Level
```

## Automations
TODO:
* Taking the photo
* Opening/closing door