import load_globals
import datetime
from astral.geocoder import database, lookup
from astral.sun import sun
import pdb

CITY_DATA = lookup(load_globals.CITY_NAME, database())


def get_sun_data(date):
    sun_data = sun(CITY_DATA.observer, date=date.date(), tzinfo=CITY_DATA.timezone)
    # Long story short, timezones suck. So we replace the timezone given from the
    # astral dict with None so that we can compare later on.
    sun_data = {key: value.replace(tzinfo=None) for key, value in sun_data.items()}
    return sun_data


def is_daytime():
    now = datetime.datetime.now()
    sun_data = get_sun_data(now)
    sunrise = get_door_open_time(sun_data["sunrise"])
    sunset = sun_data["sunset"]
    # TODO: Document better. If we want to manually shut the door in the middle of the day
    # we don't want it to just automatically open, so only open it in this window
    # return sunrise < now < (sunrise + datetime.timedelta(hours=2))
    return sunrise < now < sunset


# TODO: Figure out a better way than hardcoded 30 minutes
def get_door_open_time(sunrise):
    return sunrise + datetime.timedelta(minutes=130)


def is_nighttime():
    now = datetime.datetime.now()
    return get_door_close_time(now) < now


# Dusk seems too late, sunset seems too early, find the middle
# TODO: Ask some more versed in python how to do this better
def get_door_close_time(date):
    sun_data = get_sun_data(date)
    sunset = sun_data["sunset"]
    dusk = sun_data["dusk"]
    between_delta = (dusk - sunset) / 2
    # TODO: November around time change the chicks did not go into the coop on time
    # I need some amount of math to happen to change this close (and opening)
    # that matches something with the amount of sunlight we have, not sure
    override_timing = datetime.timedelta(minutes=20)
    return sunset + between_delta + override_timing


def pretty_format_date(date):
    return date.strftime("%I:%M %p %D")


def assert_tzinfo():
    now = datetime.datetime.now()
    dawn = get_sun_data(now)["dawn"]
    assert now.tzinfo == dawn.tzinfo


def get_pretty_formatted_sun_data():
    now = datetime.datetime.now()
    sun_data = get_sun_data(now)
    sun_data["door_close_time"] = get_door_close_time(now)
    sun_data["door_open_time"] = get_door_open_time(sun_data["sunrise"])
    sun_data = {key: pretty_format_date(value) for key, value in sun_data.items()}
    return {"sun_data": sun_data}


# pdb.set_trace()
# assert is_daytime() == True
# assert is_nighttime() == False
assert_tzinfo()
print(get_pretty_formatted_sun_data())
