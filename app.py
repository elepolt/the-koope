import load_globals

# Flask Stuff
from flask import Flask, send_file
from flask_restful import Api, Resource
from sun_data import get_pretty_formatted_sun_data, is_daytime, is_nighttime


# Camera stuff
from time import sleep
from picamera import PiCamera

# Other stuff
import pdb
import RPi.GPIO as GPIO
import signal
import datetime
import threading
import requests


# Set Broadcom mode so we can address GPIO pins by number.
GPIO.setmode(GPIO.BCM)


# Default to open
GPIO.setup(load_globals.LOWER_DOOR_SENSOR, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(load_globals.UPPER_DOOR_SENSOR, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(load_globals.MOTOR_DOWN, GPIO.OUT)
GPIO.setup(load_globals.MOTOR_UP, GPIO.OUT)
GPIO.setup(load_globals.DEBUGGER_LIGHT, GPIO.OUT)

# Thought process here: It might take longer to open the door vs close it, but I haven't tested that yet.
# So my idea is to have the general time it takes for the door to open/shut and then a secondary timer
# that should ensure it doesn't go past that. Maybe we could keep track of how long it takes in a
# database or something. I love over engineering things!
DOOR_OPENING_TIMER = 26
ITS_BEEN_A_WHILE = 35

# Some little helper functions
def GPIO_Input(gpio_pin):
    return GPIO.input(gpio_pin)


def GPIO_Output(gpio_pin, bool_value):
    return GPIO.output(gpio_pin, bool_value)


def cleanupPins():
    GPIO_Output(load_globals.MOTOR_DOWN, False)
    GPIO_Output(load_globals.MOTOR_UP, False)
    GPIO_Output(load_globals.DEBUGGER_LIGHT, False)
    GPIO.cleanup()


# Wrapping the camera functionality in its own function so that the `with` block
# can function as intended.
def image_function():
    try:
        with PiCamera() as camera:
            camera.resolution = (3280, 2464)
            camera.start_preview()

            camera_warmup_time = 5
            camera_exposure_mode = "auto"
            camera.rotation = 180
            # TODO If the time is dark, add night mode settings
            # if time > sundown:
            #     camera.shutter_speed = 6000000
            #     camera.iso = 800
            #     camera_warmup_time = 30
            #     camera_exposure_mode = "off"
            sleep(camera_warmup_time)
            camera.exposure_mode = camera_exposure_mode
            camera.annotate_text_size = 70
            camera.annotate_text = datetime.datetime.now().strftime("%D %I:%M %p")
            camera.capture("/tmp/image.jpg")
            camera.stop_preview()
    except:
        with open("error.txt", "a") as fo:
            fo.write("Camera error")
        app.logger.exception("Camera error")
        return "exception", 500


# Door sensors are 1 when disconnected and 0 when connected
# When both doors are disconnected, door is in motion
# When the door is open the upper sensor is reporting 0
def door_is_open():
    return not GPIO_Input(load_globals.UPPER_DOOR_SENSOR)


# Conversly the door is closed lower sensor is reporting 0
def door_is_closed():
    return not GPIO_Input(load_globals.LOWER_DOOR_SENSOR)


# Takes open/close argument
def open_close_wrapper(direction, is_override):
    state = "moving"
    # Sets the variables necessary to open/close the door
    if direction == "open":
        door_state = door_is_open
        motor_pin = load_globals.MOTOR_UP
        state = "open"
    elif direction == "close":
        door_state = door_is_closed
        motor_pin = load_globals.MOTOR_DOWN
        state = "closed"
    else:
        return {"state": "Not a valid option"}

    # Return the state if we try to close when the door is already closed
    # continue the function if we're overriding
    if door_state() and not is_override:
        return {"state": state, "confidence_level": "confident"}

    # Start a running clock and turn the motor in the right direction
    action_start_time = datetime.datetime.now()
    GPIO_Output(motor_pin, True)
    while True:
        ongoing_time_count = (datetime.datetime.now() - action_start_time).seconds
        # Just keep going for a couple seconds to prove we're overriding. This works in my head.
        if ongoing_time_count < 2 and is_override:
            continue

        # Once our door hits the proper state, turn the motor off and send the response
        if door_state():
            GPIO_Output(motor_pin, False)
            if direction == "close" and is_override:
                post_webhook("closed")
            # image_function() # Until I fix the camera
            return {"door": {"state": state, "confidence_level": "confident"}}

        # As a failsafe we've got a clock running that will stop the motor anyway
        if ongoing_time_count > ITS_BEEN_A_WHILE:
            GPIO_Output(motor_pin, False)
            # image_function() # Until I fix the camera
            post_webhook("error")

            return {"state": state, "confidence_level": "moderate"}


def post_webhook(type):
    if "HOME_ASSISTANT_WEBHOOK_URL" not in load_globals.local_config.keys():
        print("Home assistant webhook not set up")
        return
    if "HOME_ASSISTANT_ISSUE_WEBHOOK_URL" not in load_globals.local_config.keys():
        print("Home assistant error webhook not set up")
        return

    url = ""
    if type == "closed":
        url = load_globals.HOME_ASSISTANT_WEBHOOK_URL
    elif type == "error":
        url = load_globals.HOME_ASSISTANT_ISSUE_WEBHOOK_URL

    requests.post(url, data={}, headers={})


def door_override():
    threading.Timer(5.0, door_override).start()

    check_door_against_sun()
    check_sensor_ovverride()


# If it's the morning and the door is closed, open it
# Vice versa if the sun has gone down and the door is open, close it.
def check_door_against_sun():
    if is_daytime() and door_is_closed():
        open_close_wrapper("open", True)
    if is_nighttime() and door_is_open():
        open_close_wrapper("close", True)


# Connectivity issues can happen, if that happens we can hold a magnet to both sensors the door will close
def check_sensor_ovverride():
    if door_is_open() and door_is_closed():
        open_close_wrapper("close", True)


app = Flask(__name__)
api = Api(app)


class TheKoope(Resource):
    def get(self):
        state = "opening"
        confidence_level = "moderate"
        if door_is_closed():
            state = "closed"
            confidence_level = "confident"
        elif door_is_open():
            state = "open"
            confidence_level = "confident"

        cpu_temp = 0
        with open("/sys/class/thermal/thermal_zone0/temp", "r") as f:
            cpu_temp = f.readline()

        return {
            "door": {
                "state": state,
                "confidence_level": confidence_level,
                "door_is_open_status": door_is_open(),
                "door_is_closed_status": door_is_closed(),
                "cpu_temp": cpu_temp,
            },
            **get_pretty_formatted_sun_data(),
        }
        # } | get_pretty_formatted_sun_data() # Introduced in python 3.9, unsure of which is preferred


class KoopeDoor(Resource):
    def get(self, open_or_close):
        if open_or_close == "open":
            return open_close_wrapper("open", False)
        elif open_or_close == "close":
            return open_close_wrapper("close", False)
        return {"error": "Improper request"}


@app.route("/health_check")
def index():
    return {"status": "Ok"}


@app.route("/reset")
def reset():
    cleanupPins()
    return {"status": "Ok"}


@app.route("/get_image")
def get_image():
    return send_file("/tmp/image.jpg", mimetype="image/jpg")


@app.route("/take_image")
def take_image():
    image_function()
    return {"picture": "taken"}


@app.errorhandler(Exception)
def server_error(err):
    with open("error.txt", "a") as fo:
        fo.write(err)
    app.logger.exception(err)
    return "exception", 500


api.add_resource(TheKoope, "/")
api.add_resource(KoopeDoor, "/door/<open_or_close>")


if __name__ == "__main__":
    door_override()
    app.run(host="0.0.0.0", port=5000)
    signal.signal(signal.SIGINT, cleanupPins)
