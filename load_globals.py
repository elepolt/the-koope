import json

local_config = json.load(open("config.json"))

CITY_NAME = local_config["CITY_NAME"]
UPPER_DOOR_SENSOR = local_config["UPPER_DOOR_SENSOR"]
LOWER_DOOR_SENSOR = local_config["LOWER_DOOR_SENSOR"]
MOTOR_DOWN = local_config["MOTOR_DOWN"]
MOTOR_UP = local_config["MOTOR_UP"]
DEBUGGER_LIGHT = local_config["DEBUGGER_LIGHT"]
HOME_ASSISTANT_WEBHOOK_URL = local_config["HOME_ASSISTANT_WEBHOOK_URL"]
HOME_ASSISTANT_ISSUE_WEBHOOK_URL = local_config["HOME_ASSISTANT_ISSUE_WEBHOOK_URL"]
