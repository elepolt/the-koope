# The Koope

My chickens were murdered, so it's time to over engineer a solution to keep the new ones safe.

# Initial Item list

| Item | Cost |
| --- | --- |
| https://www.chickenguard.com/product/self-locking-door-kit/  | $40 |
| [Magnetic contact switch (door sensor) x2](https://www.adafruit.com/product/375) | $7.90 |
| [TT Motor All-Metal Gearbox](https://www.adafruit.com/product/3802) - 1:90 Gear Ratio | $5.95 |
| [DC Stepper Motor Driver Breakout Board](https://www.adafruit.com/product/3297) | $4.95 |
| [Adafruit DRV8833 DC/Stepper Motor Driver Breakout Board](https://www.adafruit.com/product/3297) | $4.95 |
| [Hammer Header Female - Solderless Raspberry Pi Connector](https://www.adafruit.com/product/3663) | $3.25 |
| Adafruit Raspberry Pi Camera Board Case with 1/4" Tripod Mount | $2.95 |
| [Raspberry Pi NoIR Camera Board v2 - 8 Megapixels](https://www.adafruit.com/product/3100) | $29.95 |
| [Adafruit STEMMA Speaker - Plug and Play Audio Amplifier](https://www.adafruit.com/product/3885) | $5.95 |
| [Circuit Playground Express](https://www.adafruit.com/product/3333) | $24.95  |
| [USB to TTL Serial Cable - Debug / Console Cable for Raspberry Pi](https://www.adafruit.com/product/954) | $9.95 |
| [Small Alligator Clip to Male Jumper Wire Bundle - 6 Pieces](https://www.adafruit.com/product/3448) | $3.95 |
| Pi Zero | $10 |
| Total | $120  |

# Local Setup
* Install virtualenv
  * `cd /path/to/the-koope`
  * `python -m venv env`
  * `source ./env/bin/activate`
  * `pip install -r requirements.txt`
  * `cp config.example.json config.json # And update to your details`
* Running the app
  * Ensure [supervisor](http://supervisord.org/installing.html) is installed (pip install should have this covered)
  * Update `koop_supervisor.conf` with your path
  * `sudo cp koop_supervisor.conf /etc/supervisor/conf.d/`
  * `sudo supervisorctl update`
* Enable camera
  * `sudo raspi-config`
  * Interface Options
  * Camera -> Yes
  * add awb_auto_is_greyworld=1 to /boot/config.txt
# Local Dev
* Ensure Flask is set up properly
  * `cd /path/to/the-koope`
  * `python app.py`
    * `export FLASK_ENV=development # Flask will restart when you update a file`
# General Notes
## Door Sensors
* work by plugging one side into ground, the other into your logic
* returns 1 while up, and 0 while down

## Motor Stepper
* You have to solder it
* There's a 'sleep' trigger that needs electricity supplied to it to work
  * Jump the power 
* Requires like 6v or something like that

## Wiring
* Yellow from 4 -> Lower Door Sensor
* Green from 17 -> Upper Door Sensor
* Ground from between 4/17 to other sensor wires
* Motor Up
  * Orange from 19 -> inA1
  * Orange from outA1 -> Motor Red
* Motor Down
  * Orange from 26 -> inA2
  * Orange from outA2 -> Motor Black

# API Endpoints
* `/`
  * Returns the door state and confidence level
* `/health_check`
  * Returns `{status: ok}`
* `/door/open`
  * Opens door until sensor is hit or user input timeout is hit
* `/door/close`
  * Closes door until sensor is hit or user input timeout is hit
* `/reset`
  * Just runs a script to set all pinouts to False
* `/take_image`
  * Takes a photo from the camera and saves it in `/tmp/image.jpeg`
* `/get_image`
  * Shows the image located at `/tmp/image.jpeg`
# Home Assistant
All details [here](HomeAssistant.md)

## Home Assistant Local Notes
* TODO: give configuration.yml details
* TODO2: Utilize volume mount and do this in the pi itself, not home assistant
* docker exec -it homeassistant bash
  * ssh-keygen
  * ssh-copy-id -i /root/.ssh/id_rsa pi@10.0.0.127
    * Enter Password
  * scp pi@10.0.0.127:/tmp/image.jpg /config/www/koope.jpg

# Ideas

* When it gets dark close the door, initiate Chicken Safety Protocols
* NOIR Camera with [motion](https://motion-project.github.io/motion_config.html#basic_setup_picam) enabled
* If there is movement play dog barking and flash lights
* Take picture of intruder
* If it gets cold turn on a heater
  * Maybe, this is a bit of a _heated_ topic
* App/website to manually close/open the door
* Another camera inside to check if all the birds are inside
